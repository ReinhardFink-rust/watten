pub const SET_COLOR_BLACK: &str = "\x1b[0m";
pub const SET_COLOR_RED: &str =  "\x1b[31m";
pub const SET_COLOR_GREEN: &str =  "\x1b[32m";
pub const SET_COLOR_YELLOW: &str =  "\x1b[33m";

pub const INTEND : usize = 4;

//pub const LABELS_COLOR : [&str; 4] = ["E:", "H:", "L:", "S:"];

//pub const LABELS_COLOR: &Vec<&str>=  &vec!["E:", "H:", "L:", "S:"];
//pub const LABELS_RANK: &Vec<&str>=  &vec!["6:", "7:", "8:", "9:","10:", "J:", "Q:", "K:", "A:"];
