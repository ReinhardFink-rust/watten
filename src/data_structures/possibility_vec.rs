use std::fmt;
use std::ops::{BitAnd, BitOr};
use crate::data_structures::constants::{SET_COLOR_BLACK, SET_COLOR_RED, SET_COLOR_GREEN, SET_COLOR_YELLOW};
use crate::data_structures::possibility_vec::Possibility::{IMPOSSIBLE, POSSIBLE, SURE};

#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub enum Possibility {
    IMPOSSIBLE,
    POSSIBLE,
    SURE,
}

impl fmt::Display for Possibility {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            IMPOSSIBLE => write!(f, "{}{}{}", SET_COLOR_RED, "IMPOSSIBLE", SET_COLOR_BLACK),
            POSSIBLE => write!(f, "{}{}{}", SET_COLOR_YELLOW, "POSSIBLE", SET_COLOR_BLACK),
            SURE => write!(f, "{}{}{}", SET_COLOR_GREEN, "SURE", SET_COLOR_BLACK),
        }
    }
}

impl BitAnd for Possibility {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        if self <= rhs {
            self
        } else {
            rhs
        }
    }
}

/* works but is highlighted red
impl BitAndAssign for Possibility {
    fn bitand_assign(&mut self, rhs: Self) {
        *self = *self & rhs;
    }
}
*/

impl BitOr for Possibility {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        if self >= rhs {
            self
        } else {
            rhs
        }
    }
}



#[derive(Debug)]
pub struct PossibilityVec {
    possibility_vec: Vec<Possibility>,
    impossible_count: usize,
    sure_at: Option<usize>,
    //labels: &'static Vec<&'static str>,
}

impl PossibilityVec {
    pub fn new(length: usize) -> Self {
        let mut possibility_vec: Vec<Possibility> = Vec::new();
        for _ in 0..length {
            possibility_vec.push(POSSIBLE);
        }
        PossibilityVec {
            possibility_vec,
            impossible_count: 0,
            // None: there is no single SURE in the array
            // Some(i): if there is a SURE at position i
            sure_at: None,
        }
    }

    /// sets a Possibility at index i:
    /// returns:
    /// Ok(i) if Possibility does not change at index i
    /// Ok(i) if Possibility changes from POSSIBLE to IMPOSSIBLE or SURE at index i
    /// Err(i) if Possibility does not change from IMPOSSIBLE or SURE to different Possibility
    /// 3 x 3 different cases have to handled
    pub fn set_p_at(&mut self, i: usize, possibility: Possibility) -> Result<usize, Option<usize>> {
        // new Possibility == old Possibility
        // 1,2,3 cases: X --> X
        if possibility == self.possibility_vec[i] {
            return Ok(i);
        }
        match possibility {
            SURE => {
                // case 4 SURE -/-> IMPOSSIBLE
                if self.possibility_vec[i] == IMPOSSIBLE {
                    return Err(self.sure_at);
                }
                // case 5 SURE --> POSSIBLE
                for mut _p in self.possibility_vec.iter_mut() {
                    *_p = IMPOSSIBLE;
                }
                self.possibility_vec[i] = SURE;
                self.impossible_count = self.possibility_vec.len() - 1;
                self.sure_at = Some(i);
                //println!("i: {} sure_at: {:?}", i, self.sure_at);
                return Ok(i);
            }
            IMPOSSIBLE => {
                // case 6 IMPOSSIBLE -/-> SURE
                if self.possibility_vec[i] == SURE {
                    return Err(self.sure_at);
                }
                // case 7 IMPOSSIBLE --> POSSIBLE
                self.possibility_vec[i] = IMPOSSIBLE;
                self.impossible_count += 1;
                // if just one POSSIBLE left POSSIBLE --> SURE
                if self.impossible_count == self.possibility_vec.len() - 1 {
                    for (i, p) in self.possibility_vec.iter().enumerate() {
                        if p == &POSSIBLE {
                            self.possibility_vec[i] = SURE;
                            self.sure_at = Some(i);
                            break;
                        }
                    }
                }
                Ok(i)
            }
            POSSIBLE => {
                // case 8, 9: POSSIBLE -/-> SURE or IMPOSSIBLE
                Err(self.sure_at)
            }
        }
    }

    pub fn get_p_at(&self, i: usize) -> Possibility {
        self.possibility_vec[i]
    }

    pub fn get_sure_at(&self) -> Option<usize> {
        //println!("sure at in fn: {:?}",self.sure_at);
        self.sure_at
    }
}

impl fmt::Display for PossibilityVec {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[").unwrap();
        for p in self.possibility_vec.iter() {
            match p {
                IMPOSSIBLE => {
                    write!(f, "{}{}{}", SET_COLOR_RED, "I", SET_COLOR_BLACK).unwrap();
                }
                POSSIBLE => {
                    write!(f, "{}{}{}", SET_COLOR_YELLOW, "P", SET_COLOR_BLACK).unwrap();
                }
                SURE => {
                    write!(f, "{}{}{}", SET_COLOR_GREEN, "S", SET_COLOR_BLACK).unwrap();
                }
            }
        }
        write!(f, "]")
    }
}

impl Clone for PossibilityVec {
    fn clone(&self) -> Self {
        let mut pa = Self::new(self.possibility_vec.len());
        for (i, p) in self.possibility_vec.iter().enumerate() {
            pa.possibility_vec[i] = *p;
        }
        pa.impossible_count = self.impossible_count;
        pa.sure_at = self.sure_at;
        pa
    }
}

impl PartialEq for PossibilityVec {
    fn eq(&self, other: &Self) -> bool {
        let equal = true;
        for (i, p) in self.possibility_vec.iter().enumerate() {
            if p != &other.possibility_vec[i] {
                return false;
            }
        }
        equal
    }
}


#[cfg(test)]
/*
#[test]
fn test_print_possibility_vec() {
    let mut pa = PossibilityVec::new(4);
    pa.set_p_at(0,Possibility::SURE).expect("TODO: panic message");
    pa.set_p_at(3,Possibility::IMPOSSIBLE).expect("TODO: panic message");
    println!("{}",pa);
}
*/
#[test]
fn test_possibility_compare() {
    assert_eq!(true, IMPOSSIBLE < POSSIBLE);
    assert_eq!(true, POSSIBLE < SURE);
    assert_eq!(true, IMPOSSIBLE < SURE);
}

#[test]
fn test_bitand() {
    assert_eq!(IMPOSSIBLE, IMPOSSIBLE & IMPOSSIBLE);
    assert_eq!(IMPOSSIBLE, IMPOSSIBLE & POSSIBLE);
    assert_eq!(IMPOSSIBLE, IMPOSSIBLE & SURE);

    assert_eq!(IMPOSSIBLE, POSSIBLE & IMPOSSIBLE);
    assert_eq!(POSSIBLE, POSSIBLE & POSSIBLE);
    assert_eq!(POSSIBLE, POSSIBLE & SURE);

    assert_eq!(IMPOSSIBLE, SURE & IMPOSSIBLE);
    assert_eq!(POSSIBLE, SURE & POSSIBLE);
    assert_eq!(SURE, SURE & SURE);
}

#[test]
fn test_bitor() {
    assert_eq!(IMPOSSIBLE, IMPOSSIBLE | IMPOSSIBLE);
    assert_eq!(POSSIBLE, IMPOSSIBLE | POSSIBLE);
    assert_eq!(SURE, IMPOSSIBLE | SURE);

    assert_eq!(POSSIBLE, POSSIBLE | IMPOSSIBLE);
    assert_eq!(POSSIBLE, POSSIBLE | POSSIBLE);
    assert_eq!(SURE, POSSIBLE | SURE);

    assert_eq!(SURE, SURE | IMPOSSIBLE);
    assert_eq!(SURE, SURE | POSSIBLE);
    assert_eq!(SURE, SURE | SURE);
}


#[test]
fn test_is_fixed_01() {
    let mut pa = PossibilityVec::new(4);
    assert_eq!(None, pa.sure_at);
    //println!("{}", pa);
    pa.set_p_at(0, SURE).expect("TODO: panic message");
    //println!("{}", pa);
    assert_eq!(Some(0), pa.sure_at);


    let mut pa = PossibilityVec::new(4);
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(3, SURE).expect("TODO: panic message");
    assert_eq!(Some(3), pa.sure_at);
}

#[test]
fn test_is_fixed_02() {
    let mut pa = PossibilityVec::new(4);
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(0, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(1, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(2, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(Some(3), pa.sure_at);


    let mut pa = PossibilityVec::new(4);
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(3, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(2, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(1, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(Some(0), pa.sure_at);


    let mut pa = PossibilityVec::new(4);
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(0, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(3, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(None, pa.sure_at);
    pa.set_p_at(2, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(Some(1), pa.sure_at);
}

#[test]
fn test_clone() {
    let mut pa = PossibilityVec::new(4);
    pa.set_p_at(0, IMPOSSIBLE).expect("TODO: panic message");
    let mut pa_clone = pa.clone();
    assert_eq!(pa, pa_clone);
    pa_clone.set_p_at(1, IMPOSSIBLE).expect("TODO: panic message");
    assert_ne!(pa, pa_clone);
    pa.set_p_at(1, IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(pa, pa_clone);
}

/*
#[test]
fn test_get_iter() {
    let mut pa= PossibilityVec::new(4);
    for p in pa.get_iter().into_iter() {
        println!("{:?}",p);
    }
}
 */
