use std::fmt;
use crate::data_structures::card::{Card, Color, Rank};

#[derive(Debug, Copy, Clone)]
pub struct Round {
    pub card_array: [Card; 4],
    pub winner_index: usize,
}

impl fmt::Display for Round {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.winner_index {
            0 => write!(f, "<\x1b[32m{}\x1b[0m, {}, {}, {}>", self.card_array[0], self.card_array[1], self.card_array[2], self.card_array[3]),
            1 => write!(f, "<{}, \x1b[32m{}\x1b[0m, {}, {}>", self.card_array[0], self.card_array[1], self.card_array[2], self.card_array[3]),
            2 => write!(f, "<{}, {}, \x1b[32m{}\x1b[0m, {}>", self.card_array[0], self.card_array[1], self.card_array[2], self.card_array[3]),
            3 => write!(f, "<{}, {}, {}, \x1b[32m{}\x1b[0m>", self.card_array[0], self.card_array[1], self.card_array[2], self.card_array[3]),
            _ => { Ok(()) }
        }
    }
}

impl Round {
    fn new(card0: Card, card1: Card, card2: Card, card3: Card, winner_index: usize) -> Self {
        Round {
            card_array: [card0, card1, card2, card3],
            winner_index,
        }
    }

    pub fn get_winner_card(&self) -> Card {
        self.card_array[self.winner_index]
    }

    // unused
    fn get_highest_rank_for_first_card_color(&self) -> Rank {
        let mut r = self.card_array[0].rank;
        for card in self.card_array.iter() {
            if card.color == self.card_array[0].color && card.rank > r {
                r = card.rank;
            }
        }
        r
    }
}

#[cfg(test)]

#[test]
fn test_get_winner_color_and_rank() {
    let mut r0 = Round::new(
        Card { color: Color::HERZ, rank: Rank::SEVEN },
        Card { color: Color::SCHELL, rank: Rank::AS },
        Card { color: Color::LAUB, rank: Rank::KING },
        Card { color: Color::HERZ, rank: Rank::JACK },
        0);
    assert_eq!(Color::HERZ, r0.get_winner_card().color);
    assert_eq!(Rank::SEVEN, r0.get_winner_card().rank);
    r0.winner_index = 1;
    assert_eq!(Color::SCHELL, r0.get_winner_card().color);
    assert_eq!(Rank::AS, r0.get_winner_card().rank);
    r0.winner_index = 2;
    assert_eq!(Color::LAUB, r0.get_winner_card().color);
    assert_eq!(Rank::KING, r0.get_winner_card().rank);
    r0.winner_index = 3;
    assert_eq!(Color::HERZ, r0.get_winner_card().color);
    assert_eq!(Rank::JACK, r0.get_winner_card().rank);
    assert_ne!(Color::SCHELL, r0.get_winner_card().color);
    assert_ne!(Rank::KING, r0.get_winner_card().rank);
}

#[test]
fn test_get_highest_rank_for_first_card_color() {
    let mut r0 = Round::new(
        Card { color: Color::HERZ, rank: Rank::SEVEN },
        Card { color: Color::SCHELL, rank: Rank::AS },
        Card { color: Color::LAUB, rank: Rank::KING },
        Card { color: Color::LAUB, rank: Rank::JACK },
        0);
    // h7,sA,lK,l7
    assert_eq!(Rank::SEVEN, r0.get_highest_rank_for_first_card_color());
    // h7,sA,lK,hJ
    r0.card_array[3] = Card { color: Color::HERZ, rank: Rank::JACK };
    assert_eq!(Rank::JACK, r0.get_highest_rank_for_first_card_color());
    // h7,hK,lK,hJ
    r0.card_array[1] = Card { color: Color::HERZ, rank: Rank::KING };
    assert_eq!(Rank::KING, r0.get_highest_rank_for_first_card_color());
    // h7,hK,hA,hJ
    r0.card_array[2] = Card { color: Color::HERZ, rank: Rank::AS };
    assert_eq!(Rank::AS, r0.get_highest_rank_for_first_card_color());

    // s6, s8, sQ, h9
    let r0 = Round::new(
        Card { color: Color::SCHELL, rank: Rank::SIX },
        Card { color: Color::SCHELL, rank: Rank::EIGHT },
        Card { color: Color::SCHELL, rank: Rank::QUEEN },
        Card { color: Color::HERZ, rank: Rank::NINE },
        0);
    assert_eq!(Rank::QUEEN, r0.get_highest_rank_for_first_card_color());
}