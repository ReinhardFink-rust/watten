use std::fmt;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Color {
    EICHEL,
    HERZ,
    LAUB,
    SCHELL,
}

#[derive(Debug, Copy, Clone, PartialEq, PartialOrd)]
pub enum Rank {
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    AS,
}

#[derive(Debug, Copy, Clone)]
pub struct Card {
    pub color: Color,
    pub rank: Rank,
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{:?}, {:?}]", self.color, self.rank)
    }
}

/*
#[cfg(test)]
#[test]
fn test_card_to_string() {
    let h7 = Card { color: Color::HERZ, rank: Rank::SEVEN };
    println!("{:?}", h7);
    let sa = Card { color: Color::SCHELL, rank: Rank::AS };
    println!("{:?}", sa);
    let lk = Card { color: Color::LAUB, rank: Rank::KING };
    println!("{:?}", lk);
    let ej = Card { color: Color::EICHEL, rank: Rank::JACK };
    println!("{:?}", ej);
}
*/
