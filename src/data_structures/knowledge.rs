use std::{fmt, mem};
use crate::data_structures::possibility_vec::{Possibility, PossibilityVec};
use crate::data_structures::card::{Card, Color, Rank};
//use crate::data_structures::constants::{BLACK, GREEN, RED, YELLOW};

#[derive(Debug, Clone)]
pub struct Knowledge {
    pub color_possibility: PossibilityVec,
    pub rank_possibility: PossibilityVec,
}

impl Knowledge {
    pub fn new() -> Self {
        Knowledge {
            color_possibility: PossibilityVec::new(mem::variant_count::<Color>()),
            rank_possibility: PossibilityVec::new(mem::variant_count::<Rank>()),
        }
    }

    pub fn set_color_possibility_at(&mut self, card: &Card, possibility: Possibility) -> Result<usize, Option<usize>> {
        self.color_possibility.set_p_at(card.color as usize, possibility)
    }

    pub fn get_color_possibility_at(&self, card: &Card) -> Possibility {
        self.color_possibility.get_p_at(card.color as usize)
    }

    pub fn set_rank_possibility_at(&mut self, card: &Card, possibility: Possibility) -> Result<usize, Option<usize>> {
        self.rank_possibility.set_p_at(card.rank as usize, possibility)
    }

    pub fn get_rank_possibility_at(&self, card: &Card) -> Possibility {
        self.rank_possibility.get_p_at(card.rank as usize)
    }
}

impl PartialEq for Knowledge {
    fn eq(&self, other: &Self) -> bool {
        self.color_possibility == other.color_possibility &&
            self.rank_possibility == other.rank_possibility
    }
}

impl fmt::Display for Knowledge {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //let labels_color = ["E:", "H:", "L:", "S:"];
        //let labels_rank = ["6:", "7:", "8:", "9:","10:", "J:", "Q:", "K:", "A:"];
        write!(f, "Farbe: {}  Schlag: {} ", self.color_possibility, self.rank_possibility)
        /*
        write!(f, "Trumpf [").unwrap();
        for (i, p) in self.color_possibility.get_iter().into_iter().enumerate() {
            println!("{:?}",p);
            match p {
                IMPOSSIBLE => {
                    write!(f, "({}{}{}{})", RED, labels_color[i], "I", BLACK).unwrap();
                },
                POSSIBLE => {
                    write!(f, "{}{}{}", YELLOW, "P", BLACK).unwrap();
                },
                SURE => {
                    write!(f, "{}{}{}", GREEN, "S", BLACK).unwrap();
                },
            }
        }
        write!(f, "]")
        */
    }
}
