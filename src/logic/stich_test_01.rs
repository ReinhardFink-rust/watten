use crate::data_structures::card::{Card, Color, Rank};
use crate::data_structures::constants::INTEND;
use crate::data_structures::knowledge::Knowledge;
use crate::data_structures::possibility_vec::Possibility;
use crate::data_structures::possibility_vec::Possibility::{IMPOSSIBLE, POSSIBLE, SURE};
use crate::data_structures::rounds::Round;
use crate::logic::{calculate_knowledge_from_round_no_good, calculate_winner_from_round};

/*
will return
 */
pub fn test_guess_reverse(recursions_level: usize, rounds: &Vec<Round>, guess: &Knowledge) -> Possibility {
    // we need 1 POSSIBLE to make recursion POSSIBLE
    let mut result = IMPOSSIBLE;
    result = result | test_for_color(recursions_level, rounds, guess);
    result = result | test_for_trumpf(recursions_level, rounds, guess);
    result = result | test_for_left(recursions_level, rounds, guess);
    result = result | test_for_right(recursions_level, rounds, guess);
    result
}

/*
Test, possibility of Farb - Stich
 */
pub fn test_for_color(recursions_level: usize, rounds: &Vec<Round>, knowledge: &Knowledge) -> Possibility {
    let mut rounds: Vec<Round> = rounds.clone().to_vec();
    let round = rounds.pop().unwrap();
    let mut result = POSSIBLE;
    print_start_message(recursions_level, knowledge, &rounds, round, "FARBSTICH");
    // create own knowledge for Farbstich
    let mut guess = knowledge.clone();
    // set guess for Farbstich
    // 1. set all colors in stitch to IMPOSSIBLE
    let mut impossible_count = 0;
    for card in round.card_array.iter() {
        impossible_count += 1;
        if let Err(_) = guess.set_color_possibility_at(card, IMPOSSIBLE) {
            match impossible_count {
                4 => {
                    print!("{}", " ".repeat(INTEND * recursions_level));
                    println!("KEIN FARBSTICH WEIL: 4 verschiedene Farben im Stich");
                }
                _ => {
                    print!("{}", " ".repeat(INTEND * recursions_level));
                    println!("KEIN FARBSTICH WEIL: {} bereits Trumpf.", card)
                }
            }
            result = IMPOSSIBLE
        }
    }
    // 2. set all ranks in stitch to IMPOSSIBLE
    if result != IMPOSSIBLE {
        for card in round.card_array.iter() {
            if let Err(_) = guess.set_rank_possibility_at(card, IMPOSSIBLE) {
                print!("{}", " ".repeat(INTEND * recursions_level));
                println!("KEIN FARBSTICH WEIL: {} ist Schlag.", card);
                result = IMPOSSIBLE;
            }
        }
    }
    // 3. test if current guess is possible in this & former rounds
    if result != IMPOSSIBLE {
        result = compare_calculated_winner_with_winner(
            recursions_level,
            &rounds.len() + 1,
            &round,
            &guess,
            "FARBSTICH");
    }
    // 4. test recursive if current guess conflicts with former stiche
    if result != IMPOSSIBLE &&
        rounds.len() > 0 &&
        test_guess_reverse(recursions_level + 1, &rounds, &guess) == IMPOSSIBLE {
        print!("{}", " ".repeat(INTEND * recursions_level));
        println!("KEIN FARBSTICH WEIL: Widerspruch zu einer vorhergehenden Runde");
        result = IMPOSSIBLE;
    }
    if knowledge == &guess && result == POSSIBLE {
        result = SURE;
    }
    print_result_message(recursions_level, &result, round, &guess, "FARBSTICH");
    result
}

/*
Test, possibility of Trumpf - Stich
 */
pub fn test_for_trumpf(recursions_level: usize, rounds: &Vec<Round>, knowledge: &Knowledge) -> Possibility {
    let mut rounds: Vec<Round> = rounds.clone().to_vec();
    let round = rounds.pop().unwrap();
    let mut result = POSSIBLE;
    print_start_message(recursions_level, knowledge, &rounds, round, "TRUMPFSTICH");
    // create own knowledge for Trumpfstich
    let mut guess = knowledge.clone();
    // set guess for Trumpfstich
    // 1. set winner card color in Stich to SURE
    let winner_card = &round.get_winner_card();
    if let Err(has_color_sure_at) = guess.set_color_possibility_at(winner_card, SURE) {
        match has_color_sure_at {
            // can not set to IMPOSSIBLE but no SURE
            None => {
                print!("{}", " ".repeat(INTEND * recursions_level));
                println!("KEIN TRUMPFSTICH WEIL: \
                    {:?} als Trumpf unmöglich.", winner_card.color);
                result = IMPOSSIBLE;
            }
            // can not set to IMPOSSIBLE but with fixed SURE
            Some(trumpf) => {
                print!("{}", " ".repeat(INTEND * recursions_level));
                println!("KEIN TRUMPFSTICH WEIL: \
                    {:? } bereits Trumpf.", round.card_array[trumpf].color);
                result = IMPOSSIBLE;
            }
        }
    }
    // 2. set all ranks in stitch to IMPOSSIBLE
    if result != IMPOSSIBLE {
        for card in round.card_array.iter() {
            if let Err(_) = guess.set_rank_possibility_at(card, IMPOSSIBLE) {
                print!("{}", " ".repeat(INTEND * recursions_level));
                println!("KEIN TRUMPFSTICH WEIL: {} ist Schlag.", card);
                result = IMPOSSIBLE;
            }
        }
    }
    // 3. test if current guess is possible in this & former rounds
    if result != IMPOSSIBLE {
        result = compare_calculated_winner_with_winner(
            recursions_level,
            &rounds.len() + 1,
            &round,
            &guess,
            "TRUMPFSTICH");
    }
    // 4. test recursive if current guess conflicts with former stitches
    if result != IMPOSSIBLE &&
        rounds.len() > 0 &&
        test_guess_reverse(recursions_level + 1, &rounds, &guess) == IMPOSSIBLE {
        print!("{}", " ".repeat(INTEND * recursions_level));
        println!("KEIN TRUMPFSTICH WEIL: Widerspruch zu einer vorhergehenden Runde");
        result = IMPOSSIBLE;
    }
    if knowledge == &guess && result == POSSIBLE {
        result = SURE;
    }
    print_result_message(recursions_level, &result, round, &guess, "TRUMPFSTICH");
    result
}

pub fn test_for_left(recursions_level: usize, rounds: &Vec<Round>, knowledge: &Knowledge) -> Possibility {
    let mut rounds: Vec<Round> = rounds.clone().to_vec();
    let round = rounds.pop().unwrap();
    let mut result = POSSIBLE;
    print_start_message(recursions_level, knowledge, &rounds, round, "LINKERSTICH");
    // create own knowledge for Linkerstich
    let mut guess = knowledge.clone();
    // set guess for Linkerstich
    // 1. set winner card color in stitch to IMPOSSIBLE
    let winner_card = &round.get_winner_card();
    if let Err(has_coöor_sure_at) = guess.set_color_possibility_at(winner_card, IMPOSSIBLE) {
        print!("{}", " ".repeat(INTEND * recursions_level));
        println!("KEIN LINKERSTICH WEIL: {:? } bereits Trumpf.",
                 has_coöor_sure_at.unwrap());
        result = IMPOSSIBLE;
    }
    // 2. set rank for winner card in stitch to SURE
    if result != IMPOSSIBLE {
        if let Err(has_rank_sure_at) = guess.set_rank_possibility_at(winner_card, SURE) {
            print!("{}", " ".repeat(INTEND * recursions_level));
            println!("KEIN LINKERSTICH WEIL: {:?} ist bereits Schlag.",
                     has_rank_sure_at.unwrap());
            result = IMPOSSIBLE;
        }
    }
    // 3. test if current guess is possible in this & former rounds
    if result != IMPOSSIBLE {
        result = compare_calculated_winner_with_winner(
            recursions_level,
            &rounds.len() + 1,
            &round,
            &guess,
            "LINKERSTICH");
    }
    // 4. test recursive if current guess conflicts with former stitches
    if result != IMPOSSIBLE &&
        rounds.len() > 0 &&
        test_guess_reverse(recursions_level + 1, &rounds, &guess) == IMPOSSIBLE {
        print!("{}", " ".repeat(INTEND * recursions_level));
        println!("KEIN LINKERSTICH WEIL: Widerspruch zu einer vorhergehenden Runde");
        result = IMPOSSIBLE;
    }
    if knowledge == &guess && result == POSSIBLE {
        result = SURE;
    }
    print_result_message(recursions_level, &result, round, &guess, "LINKERSTICH");
    result
}

pub fn test_for_right(recursions_level: usize, rounds: &Vec<Round>, knowledge: &Knowledge) -> Possibility {
    let mut rounds: Vec<Round> = rounds.clone().to_vec();
    let round = rounds.pop().unwrap();
    let mut result = POSSIBLE;
    print_start_message(recursions_level, knowledge, &rounds, round, "RECHTERSTICH");
    // create own knowledge for Rechterstich
    let mut guess = knowledge.clone();
    // set guess for Rechterstich
    // 1. set winner card color in stitch to IMPOSSIBLE
    let winner_card = &round.get_winner_card();
    //println!("{}",winner_card);
    if let Err(_) = guess.set_color_possibility_at(winner_card, SURE) {
        match guess.color_possibility.get_sure_at() {
            None => {}
            Some(trumpf) => {
                print!("{}", " ".repeat(INTEND * recursions_level));
                println!("KEIN RECHTERSTICH WEIL: \
                    {:? } bereits Trumpf.", round.card_array[trumpf].color);
                result = IMPOSSIBLE;
            }
        }
    }
    // 2. set rank for winner card in stitch to SURE
    if result != IMPOSSIBLE {
        if let Err(_) = guess.set_rank_possibility_at(winner_card, SURE) {
            match guess.color_possibility.get_sure_at() {
                None => {}
                Some(schlag) => {
                    print!("{}", " ".repeat(INTEND * recursions_level));
                    println!("KEIN RECHTERSTICH WEIL: {} ist bereits Schlag.", round.card_array[schlag]);
                    result = IMPOSSIBLE;
                }
            }
        }
    }
    // 3. test if current guess is possible in this & former rounds
    if result != IMPOSSIBLE {
        result = compare_calculated_winner_with_winner(
            recursions_level,
            &rounds.len() + 1,
            &round,
            &guess,
            "LINKERSTICH");
    }
    // 4. test recursive if current guess conflicts with former atitches
    if result != IMPOSSIBLE &&
        rounds.len() > 0 &&
        test_guess_reverse(recursions_level + 1, &rounds, &guess) == IMPOSSIBLE {
        print!("{}", " ".repeat(INTEND * recursions_level));
        println!("KEIN RECHTERSTICH WEIL: Widerspruch zu einer vorhergehenden Runde");
        result = IMPOSSIBLE;
    }
    if knowledge == &guess && result == POSSIBLE {
        result = SURE;
    }
    print_result_message(recursions_level, &result, round, &guess, "RECHTERSTICH");
    result
}

pub fn compare_calculated_winner_with_winner(recursions_level: usize,
                                         rounds_count: usize,
                                         round: &Round,
                                         guess: &Knowledge,
                                         stitch_type: &str) -> Possibility {
    let calculated_winner = calculate_winner_from_round(&round, &guess);
    if calculated_winner != round.winner_index {
        print!("{}", " ".repeat(INTEND * recursions_level));
        println!("KEIN {} WEIL: In Runde\x1b[33m {}\x1b[0m \
                    lautet der Gewinner \x1b[32m {}\x1b[0m \
                    anstatt \x1b[31m {} \x1b[0m.",
                 stitch_type,
                 rounds_count,
                 round.card_array[round.winner_index],
                 round.card_array[calculated_winner]
        );
        return IMPOSSIBLE;
    }
    POSSIBLE
}

pub fn print_start_message(recursions_level: usize, knowledge: &Knowledge, rounds: &Vec<Round>, round: Round, stitch_type: &str) {
    print!("{}", " ".repeat(INTEND * recursions_level));
    println!("START TEST {} für Stich #{}: {}", stitch_type, rounds.len() + 1, round);
    print!("{}", " ".repeat(INTEND * recursions_level));
    println!("mit Vorwissen: {}", knowledge);
}

pub fn print_result_message(recursions_level: usize, is_farb_stich: &Possibility, round: Round, guess: &Knowledge, stitch_type: &str) {
    print!("{}", " ".repeat(INTEND * recursions_level));
    println!("ERGEBNIS TEST {} für Stich:", stitch_type);
    match is_farb_stich {
        POSSIBLE => {
            print!("{}", " ".repeat(INTEND * recursions_level));
            println!("{} mit Annahme {} ist: \x1b[33m{}\x1b[0m: \n",
                     round, guess, is_farb_stich)
        }
        SURE => {
            print!("{}", " ".repeat(INTEND * recursions_level));
            println!("{} mit Annahme {} ist: \x1b[32m{}\x1b[0m: \n",
                     round, guess, is_farb_stich)
        }
        IMPOSSIBLE => {
            print!("{}", " ".repeat(INTEND * recursions_level));
            println!("{} mit Annahme {} ist: \x1b[31m{}\x1b[0m: \n",
                     round, guess, is_farb_stich)
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
#[test]
fn test_01_all_1_round_no_knowledge() {
    println!("#################### Round #1 ####################");
    println!();
    println!("#################### Test 01 ####################");
    println!("keine zusätzliche Information");
    println!("l7, s9, h10, lJ, 3 Farbstich möglich, weil lJ schlägt l7");
    println!("l7, s9, h10, lJ, 3 Trumpfstich möglich, weil lJ schlägt l7");
    println!("l7, s9, h10, lJ, 3 Linkerstich möglich, weil lJ möglicher Linker");
    println!("l7, s9, h10, lJ, 3 Rechterstich möglich, weil lJ möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(POSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 02 ####################");
    println!("keine zusätzliche Information");
    println!("e7, s9, h10, lJ, 0 ist kein Farbstich, weil 4 Farben im Stich");
    println!("e7, s9, h10, lJ, 0 Trumpfstich möglich, weil Eichel möglicher Trumpf");
    println!("e7, s9, h10, lJ, 0 Linkerstich möglich, weil 7 möglicher Linker");
    println!("e7, s9, h10, lJ, 0 Rectererstich möglich, weil 7 möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::EICHEL, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 0,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 03 ####################");
    println!("keine zusätzliche Information");
    println!("l7, s9, s10, eJ, 3 ist kein Farbstich, weil eJ schlägt l7");
    println!("l7, s9, s10, eJ, 3 Trumpfstich möglich, weil eJ möglicher Trumpf");
    println!("l7, s9, h10, eJ, 3 Linkerstich möglich, weil eJ möglicher Linker");
    println!("l7, s9, h10, eJ, 3 Rechterstich möglich, weil eJ möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 04 ####################");
    println!("keine zusätzliche Information");
    println!("l7, s10, hJ, lJ, 2 ist kein Farbstich, hJ schlägt l7");
    println!("l7, s10, hJ, lJ, 2 Trumpfstich möglich, da Linker Herz Trumpf sein kann");
    println!("l7, s10, hJ, lJ, 2 Linkerstich möglich, weil hJ Linker sein kann");
    println!("l7, s10, hJ, lJ, 2 Rechterstich möglich, weil hJ Rechter sein kann");
    let mut knowledge = Knowledge::new();
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::HERZ, rank: Rank::JACK },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 2,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));
}

#[test]
fn test_02_all_1_round_with_knowledge() {
    println!("#################### Test 01 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, s8, h10, lK, 1 ist kein Farbstich, weil Schell bereits Trumpf");
    println!("l7, s8, h10, lK, 1 Trumpfstich sicher, s8 einziger Trumpf und kein Schlag");
    println!("l7, s8, h10, lK, 3 Linkerstich unmöglich, weil kein J im Stich");
    println!("l7, s8, h10, lK, 3 Rechterstich unmöglich, weil kein J im Stich");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::EIGHT },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::KING }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 02 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, hJ, h10, lK, 1 ist kein Farbstich, hJ schlägt l7");
    println!("l7, hJ, h10, lK, 1 kein Trumpfstich,  da Linker hJ sticht");
    println!("l7, hJ, h10, lK, 1 Linkerstich sicher, weil hJ Linker");
    println!("l7, hJ, h10, lK, 1 Rechtererstich unmöglich, weil hJ Linker");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    // l7, s8, h10, lK, 1 ist kein Farbstich, weil hJ = Linker schlägt lK
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::HERZ, rank: Rank::JACK },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::KING }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 03 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, sJ, h10, lK, 1 ist kein Farbstich, sJ schlägt l7");
    println!("l7, sJ, h10, lK, 1 kein Trumpfstich,  da Rechter sJ sticht");
    println!("l7, sJ, h10, lK, 1 Linkerstich unmöglich, weil sJ Rechter");
    println!("l7, sJ, h10, lK, 1 Rechtererstich sicher, weil sJ Rechter");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    // l7, s8, h10, lK, 1 ist kein Farbstich, weil sJ = Rechter schlägt lK
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::JACK },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::KING }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 04 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, s10, h10, lJ, 3 ist kein Farbstich, lJ ist Linker");
    println!("l7, s10, h10, lJ, 3 kein Trumpfstich,  da Linker lJ Trumpf s10 sticht");
    println!("l7, s10, h10, lJ, 3 Linkerstich sicher, weil lJ Linker");
    println!("l7, s10, h10, lJ, 3 Rechtererstich unmöglich, weil lJ Linker");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 05 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, s10, hJ, lJ, 2 ist kein Farbstich, hJ ist Linker");
    println!("l7, s10, hJ, lJ, 2 kein Trumpfstich,  da Linker hJ Trumpf s10 sticht");
    println!("l7, s10, hJ, lJ, 2 Linkerstich sicher, weil hJ Linker");
    println!("l7, s10, hJ, lJ, 2 Rechterstich unmöglich, weil hJ Linker");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::HERZ, rank: Rank::JACK },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 2,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));
}

#[test]
fn test_03_all_2_rounds_no_knowledge() {
    println!("#################### Round #1 & #2 ####################");
    println!();
    println!("#################### Test 11 ####################");
    println!("Stich #1:");
    println!("keine zusätzliche Information, aber s9 als geheimer Rechter");
    println!("l7, l9, s10, eJ, 1 Farbstich möglich, weil l9 schlägt l7");
    println!("l7, l9, s10, eJ, 1 Trumpfstich möglich, weil l9 schlägt l7");
    println!("l7, l9, s10, eJ, 1 Linkerstich möglich, weil l9 möglicher Linker");
    println!("l7, l9, s10, eJ, 1 Rechterstich möglich, weil l9 möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::LAUB, rank: Rank::NINE },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 1,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let mut rs = vec![r];
    assert_eq!(POSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("Stich #2:");
    println!("keine zusätzliche Information, außer Stich #1");
    println!("sQ, sK, l10, e7, 1 Farbstich möglich, \
                weil sK schlägt sQ, und HERZ könnte Trumpf sein");
    println!("sQ, sK, l10, e7, 1 Trumpfstich möglich, \
                aber l9 Linker & Rechter");
    println!("sQ, sK, l10, e7, 1 Linkerstich möglich, \
                aber dann LAUB Trumpf");
    println!("sQ, sK, l10, e7, 1 Rechterstich unmöglich, \
                dann müsste im ersten Stich s10 gewinnen");
    let r = Round {
        card_array: [
            Card { color: Color::SCHELL, rank: Rank::QUEEN },
            Card { color: Color::SCHELL, rank: Rank::KING },
            Card { color: Color::LAUB, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::SEVEN }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    rs.push(r);
    assert_eq!(POSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 12 ####################");
    println!("Stich #1:");
    println!("keine zusätzliche Information, aber s9 als geheimer Rechter");
    println!("h8, h10, s10, sJ, 3 kein Farbstich möglich, weil sJ schlägt h8");
    println!("h8, h10, s10, sJ, 3 Trumpfstich möglich, weil sJ schlägt h8");
    println!("h8, h10, s10, sJ, 3 Linkerstich möglich, weil sJ möglicher Linker");
    println!("h8, h10, s10, sJ, 3 Rechterstich möglich, weil sJ möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::HERZ, rank: Rank::EIGHT },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::SCHELL, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let mut rs = vec![r];
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("Stich #2:");
    println!("keine zusätzliche Information, außer Stich #1");
    println!("hA, s7, sQ, eJ, 2 Farbstich unmöglich, weil sQ schlägt hA");
    println!("hA, s7, sQ, eJ, 2 Trumpfstich möglich, weil s möglicher Trumpf");
    println!("hA, s7, sQ, eJ, 2 Linkerstich unmöglich, weil s dann kein Trumpf und s10 sclägt h10 in #1");
    println!("hA, s7, sQ, eJ, 2 Recterstich möglich, weil sQ könnte Rechter sein");
    let r = Round {
        card_array: [
            Card { color: Color::HERZ, rank: Rank::AS },
            Card { color: Color::SCHELL, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::QUEEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 2,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    rs.push(r);
    assert_eq!(IMPOSSIBLE, test_for_color(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));
}

