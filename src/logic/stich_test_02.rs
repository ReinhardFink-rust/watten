
use crate::data_structures::card::{Card, Color, Rank};
use crate::data_structures::constants::INTEND;
use crate::data_structures::knowledge::Knowledge;
use crate::data_structures::possibility_vec::Possibility;
use crate::data_structures::possibility_vec::Possibility::{IMPOSSIBLE, POSSIBLE, SURE};
use crate::data_structures::rounds::Round;
use crate::logic::{calculate_knowledge_from_round_no_good};
use crate::logic::stich_test_01::{compare_calculated_winner_with_winner, print_result_message, test_for_left, test_for_right, test_for_trumpf, test_guess_reverse};

trait StitchTest {
    fn print_start_message(&self);
    fn print_end_message(&mut self);
    fn set_color(&mut self);
    fn set_rank(&mut self);
    fn check_current_stitch(&mut self);
    fn check_former_stitches(&mut self);
    fn run_test(&mut self) -> Possibility;
}

struct ColorStitch {
    rounds_left : Vec<Round>,
    round : Round,
    knowledge : Knowledge,
    guess : Knowledge,
    result : Possibility,
    recursions_level : usize
}

impl ColorStitch {

    fn new(rounds: &Vec<Round>, knowledge: &Knowledge, recursions_level: usize) -> Self {
        let mut rounds_left = rounds.clone().to_vec();
        let knowledge= knowledge.clone();
        let guess = knowledge.clone();
        let round = rounds_left.pop().unwrap();
        let result = POSSIBLE;

        ColorStitch {
            rounds_left,
            round,
            knowledge,
            guess,
            result,
            recursions_level,
        }
    }
}

impl StitchTest for ColorStitch {

    fn print_start_message(&self) {
        let stitch_type = "FARBSTICH";
        print!("{}", " ".repeat(INTEND * self.recursions_level));
        println!("START TEST {} für Stich #{}: {}", stitch_type, self.rounds_left.len() + 1, self.round);
        print!("{}", " ".repeat(INTEND * self.recursions_level));
        println!("mit Vorwissen: {}", self.guess);

    }

    fn print_end_message(&mut self) {
        let stitch_type = "FARBSTICH";
        if self.knowledge == self.guess && self.result == POSSIBLE {
            self.result = SURE;
        }
        print_result_message(self.recursions_level, &self.result, self.round, &self.guess, stitch_type);
    }

    fn set_color(&mut self) {
        let mut impossible_count = 0;
        for card in self.round.card_array.iter() {
            impossible_count += 1;
            if let Err(_) = self.guess.set_color_possibility_at(card, IMPOSSIBLE) {
                match impossible_count {
                    4 => {
                        print!("{}", " ".repeat(INTEND * self.recursions_level));
                        println!("KEIN FARBSTICH WEIL: 4 verschiedene Farben im Stich")
                    }
                    _ => {
                        print!("{}", " ".repeat(INTEND * self.recursions_level));
                        println!("KEIN FARBSTICH WEIL: {} bereits Trumpf.", card)
                    }
                }
                self.result = IMPOSSIBLE
            }
        }
    }

    fn set_rank(&mut self) {
        for card in self.round.card_array.iter() {
            if let Err(_) = self.guess.set_rank_possibility_at(card, IMPOSSIBLE) {
                print!("{}", " ".repeat(INTEND * self.recursions_level));
                println!("KEIN FARBSTICH WEIL: {} ist Schlag.", card);
                self.result = IMPOSSIBLE;
            }
        }
    }

    fn check_current_stitch(&mut self)  {
        let stitch_type = "FARBSTICH";
        self.result = compare_calculated_winner_with_winner(
            self.recursions_level,
            &self.rounds_left.len() + 1,
            &self.round,
            &self.guess,
            stitch_type);
    }

    fn check_former_stitches(&mut self) {
        let stitch_type = "FARBSTICH";
        if self.rounds_left.len() > 0 &&
            test_guess_reverse(self.recursions_level + 1, &self.rounds_left, &self.guess) == IMPOSSIBLE {
            print!("{}", " ".repeat(INTEND * self.recursions_level));
            println!("KEIN {} WEIL: Widerspruch zu einer vorhergehenden Runde", stitch_type);
            self.result = IMPOSSIBLE;
        }
    }

    fn run_test(&mut self) -> Possibility {
        self.print_start_message();
        self.set_color();
        if self.result != IMPOSSIBLE {
            self.set_rank();
            if self.result != IMPOSSIBLE {
                self.check_current_stitch();
                if self.result != IMPOSSIBLE {
                    self.check_former_stitches();
                }
            }
        }
        self.set_rank();
        if self.result == IMPOSSIBLE {
            return IMPOSSIBLE
        }
        self.print_end_message();
        self.result
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
#[test]
fn test_01_all_1_round_no_knowledge() {
    println!("#################### Round #1 ####################");
    println!();
    println!("#################### Test 01 ####################");
    println!("keine zusätzliche Information");
    println!("l7, s9, h10, lJ, 3 Farbstich möglich, weil lJ schlägt l7");
    println!("l7, s9, h10, lJ, 3 Trumpfstich möglich, weil lJ schlägt l7");
    println!("l7, s9, h10, lJ, 3 Linkerstich möglich, weil lJ möglicher Linker");
    println!("l7, s9, h10, lJ, 3 Rechterstich möglich, weil lJ möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(POSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 02 ####################");
    println!("keine zusätzliche Information");
    println!("e7, s9, h10, lJ, 0 ist kein Farbstich, weil 4 Farben im Stich");
    println!("e7, s9, h10, lJ, 0 Trumpfstich möglich, weil Eichel möglicher Trumpf");
    println!("e7, s9, h10, lJ, 0 Linkerstich möglich, weil 7 möglicher Linker");
    println!("e7, s9, h10, lJ, 0 Rectererstich möglich, weil 7 möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::EICHEL, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 0,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 03 ####################");
    println!("keine zusätzliche Information");
    println!("l7, s9, s10, eJ, 3 ist kein Farbstich, weil eJ schlägt l7");
    println!("l7, s9, s10, eJ, 3 Trumpfstich möglich, weil eJ möglicher Trumpf");
    println!("l7, s9, h10, eJ, 3 Linkerstich möglich, weil eJ möglicher Linker");
    println!("l7, s9, h10, eJ, 3 Rechterstich möglich, weil eJ möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 04 ####################");
    println!("keine zusätzliche Information");
    println!("l7, s10, hJ, lJ, 2 ist kein Farbstich, hJ schlägt l7");
    println!("l7, s10, hJ, lJ, 2 Trumpfstich möglich, da Linker Herz Trumpf sein kann");
    println!("l7, s10, hJ, lJ, 2 Linkerstich möglich, weil hJ Linker sein kann");
    println!("l7, s10, hJ, lJ, 2 Rechterstich möglich, weil hJ Rechter sein kann");
    let mut knowledge = Knowledge::new();
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::HERZ, rank: Rank::JACK },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 2,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));
}

#[test]
fn test_02_all_1_round_with_knowledge() {
    println!("#################### Test 01 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, s8, h10, lK, 1 ist kein Farbstich, weil Schell bereits Trumpf");
    println!("l7, s8, h10, lK, 1 Trumpfstich sicher, s8 einziger Trumpf und kein Schlag");
    println!("l7, s8, h10, lK, 3 Linkerstich unmöglich, weil kein J im Stich");
    println!("l7, s8, h10, lK, 3 Rechterstich unmöglich, weil kein J im Stich");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::EIGHT },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::KING }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(SURE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 02 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, hJ, h10, lK, 1 ist kein Farbstich, hJ schlägt l7");
    println!("l7, hJ, h10, lK, 1 kein Trumpfstich,  da Linker hJ sticht");
    println!("l7, hJ, h10, lK, 1 Linkerstich sicher, weil hJ Linker");
    println!("l7, hJ, h10, lK, 1 Rechtererstich unmöglich, weil hJ Linker");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    // l7, s8, h10, lK, 1 ist kein Farbstich, weil hJ = Linker schlägt lK
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::HERZ, rank: Rank::JACK },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::KING }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 03 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, sJ, h10, lK, 1 ist kein Farbstich, sJ schlägt l7");
    println!("l7, sJ, h10, lK, 1 kein Trumpfstich,  da Rechter sJ sticht");
    println!("l7, sJ, h10, lK, 1 Linkerstich unmöglich, weil sJ Rechter");
    println!("l7, sJ, h10, lK, 1 Rechtererstich sicher, weil sJ Rechter");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    // l7, s8, h10, lK, 1 ist kein Farbstich, weil sJ = Rechter schlägt lK
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::JACK },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::KING }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 04 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, s10, h10, lJ, 3 ist kein Farbstich, lJ ist Linker");
    println!("l7, s10, h10, lJ, 3 kein Trumpfstich,  da Linker lJ Trumpf s10 sticht");
    println!("l7, s10, h10, lJ, 3 Linkerstich sicher, weil lJ Linker");
    println!("l7, s10, h10, lJ, 3 Rechtererstich unmöglich, weil lJ Linker");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("#################### Test 05 ####################");
    println!("Vorwissen: Rechter = sJ");
    println!("l7, s10, hJ, lJ, 2 ist kein Farbstich, hJ ist Linker");
    println!("l7, s10, hJ, lJ, 2 kein Trumpfstich,  da Linker hJ Trumpf s10 sticht");
    println!("l7, s10, hJ, lJ, 2 Linkerstich sicher, weil hJ Linker");
    println!("l7, s10, hJ, lJ, 2 Rechterstich unmöglich, weil hJ Linker");
    let s_j = Card { color: Color::SCHELL, rank: Rank::JACK };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&s_j, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&s_j, SURE).expect("TODO: panic message");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::HERZ, rank: Rank::JACK },
            Card { color: Color::LAUB, rank: Rank::JACK }
        ],
        winner_index: 2,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let rs = vec![r];
    assert_eq!(IMPOSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(IMPOSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(SURE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));
}

#[test]
fn test_03_all_2_rounds_no_knowledge() {
    println!("#################### Round #1 & #2 ####################");
    println!();
    println!("#################### Test 11 ####################");
    println!("Stich #1:");
    println!("keine zusätzliche Information, aber s9 als geheimer Rechter");
    println!("l7, l9, s10, eJ, 1 Farbstich möglich, weil l9 schlägt l7");
    println!("l7, l9, s10, eJ, 1 Trumpfstich möglich, weil l9 schlägt l7");
    println!("l7, l9, s10, eJ, 1 Linkerstich möglich, weil l9 möglicher Linker");
    println!("l7, l9, s10, eJ, 1 Rechterstich möglich, weil l9 möglicher Rechter");
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::LAUB, rank: Rank::NINE },
            Card { color: Color::SCHELL, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 1,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let mut rs = vec![r];
    assert_eq!(POSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_right(0, &rs, &knowledge));

    println!("Stich #2:");
    println!("keine zusätzliche Information, außer Stich #1");
    println!("sQ, sK, l10, e7, 1 Farbstich möglich, \
                weil sK schlägt sQ, und HERZ könnte Trumpf sein");
    println!("sQ, sK, l10, e7, 1 Trumpfstich möglich, \
                aber l9 Linker & Rechter");
    println!("sQ, sK, l10, e7, 1 Linkerstich möglich, \
                aber dann LAUB Trumpf");
    println!("sQ, sK, l10, e7, 1 Rechterstich unmöglich, \
                dann müsste im ersten Stich s10 gewinnen");
    let r = Round {
        card_array: [
            Card { color: Color::SCHELL, rank: Rank::QUEEN },
            Card { color: Color::SCHELL, rank: Rank::KING },
            Card { color: Color::LAUB, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::SEVEN }
        ],
        winner_index: 1,
    };
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    rs.push(r);
    assert_eq!(POSSIBLE, ColorStitch::new(&rs, &knowledge, 0).run_test());
    assert_eq!(POSSIBLE, test_for_trumpf(0, &rs, &knowledge));
    assert_eq!(POSSIBLE, test_for_left(0, &rs, &knowledge));
    assert_eq!(IMPOSSIBLE, test_for_right(0, &rs, &knowledge));
}


