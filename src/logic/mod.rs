mod stich_test_01;
mod stich_test_02;

use crate::data_structures::knowledge::Knowledge;
use crate::data_structures::possibility_vec::Possibility::{IMPOSSIBLE, SURE};
use crate::data_structures::rounds::Round;
use crate::data_structures::card::{Card, Color, Rank};

//TODO:
// das muss doch besser gehen :-(
fn is_second_winner(first: &Card, second: &Card, guess: &Knowledge) -> bool {
    // Rechter/Linker
    if guess.get_rank_possibility_at(first) == SURE {
        if guess.get_rank_possibility_at(second) == SURE
            && guess.get_color_possibility_at(second) == SURE {
            // second == Rechter
            true
        } else {
            // first Rechter oder erster Linker
            false
        }
    } else if guess.get_rank_possibility_at(second) == SURE {
        // second == Linker
        true
        // ab hier kein Schlag mehr möglich
    } else if guess.get_color_possibility_at(first) == SURE {
        // Trumpfstich first möglich
        if guess.get_color_possibility_at(second) == SURE {
            // Trumpf <-> Trumpf
            first.rank < second.rank
        } else {
            // Trumpf <-> Farbe
            false
        }
    } else if guess.get_color_possibility_at(second) == SURE {
        // Farbe <-> Trumpf
        true
    } else if first.color == second.color {
        // Farbe <-> Farbe
        first.rank < second.rank
    } else {
        false
    }
}

fn calculate_knowledge_from_round_no_good(round: &Round, knowledge: &mut Knowledge) {
    let winner_card = round.get_winner_card();
    for card in round.card_array.iter() {
        if card.rank != winner_card.rank {
            knowledge.set_rank_possibility_at(card, IMPOSSIBLE).expect("TODO: panic message");
        }
    }
}

fn calculate_winner_from_round(round: &Round, knowledge: &Knowledge) -> usize {
    let mut winner = 0;
    for i in 1..round.card_array.len() {
        if is_second_winner(&round.card_array[winner], &round.card_array[i], knowledge) {
            //println!("{:?}  {:?}",&round.cards[winner],&round.cards[i]);
            winner = i;
        }
    }
    winner
}



///////////////////////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
#[test]
fn test_first_beats_second() {
    // Rechter == eK
    let e_k = Card { color: Color::EICHEL, rank: Rank::KING };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&e_k, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&e_k, SURE).expect("TODO: panic message");

    // Rechter schlägt Linker
    let h_k = Card { color: Color::HERZ, rank: Rank::KING };
    assert_eq!(false, is_second_winner(&e_k, &h_k, &knowledge));
    assert_eq!(true, is_second_winner(&h_k, &e_k, &knowledge));

    // Rechter schlägt Trumpf
    let h_a = Card { color: Color::HERZ, rank: Rank::AS };
    assert_eq!(false, is_second_winner(&e_k, &h_a, &knowledge));
    assert_eq!(true, is_second_winner(&h_a, &e_k, &knowledge));

    // erster Linker schlägt Linker
    let l_k = Card { color: Color::LAUB, rank: Rank::KING };
    assert_eq!(false, is_second_winner(&l_k, &h_k, &knowledge));
    assert_eq!(false, is_second_winner(&h_k, &l_k, &knowledge));

    // Linker schlägt Trumpf
    assert_eq!(false, is_second_winner(&l_k, &h_a, &knowledge));
    assert_eq!(true, is_second_winner(&h_a, &l_k, &knowledge));

    // Trumpf schlägt Trumpf
    let e_8 = Card { color: Color::EICHEL, rank: Rank::EIGHT };
    let e_9 = Card { color: Color::EICHEL, rank: Rank::NINE };
    assert_eq!(false, is_second_winner(&e_9, &e_8, &knowledge));
    assert_eq!(true, is_second_winner(&e_8, &e_9, &knowledge));

    // Trumpf schlägt Farbe
    assert_eq!(false, is_second_winner(&e_9, &h_a, &knowledge));
    assert_eq!(true, is_second_winner(&h_a, &e_9, &knowledge));

    // Farbe schlägt gleiche Farbe
    let h_j = Card { color: Color::HERZ, rank: Rank::JACK };
    assert_eq!(false, is_second_winner(&h_a, &h_j, &knowledge));
    assert_eq!(true, is_second_winner(&h_j, &h_a, &knowledge));

    // erste Farbe schlägt nächste Farbe
    let s_10 = Card { color: Color::SCHELL, rank: Rank::JACK };
    assert_eq!(false, is_second_winner(&s_10, &h_j, &knowledge));
    assert_eq!(false, is_second_winner(&h_j, &s_10, &knowledge));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#[test]
fn test_calculate_knowledge_from_round_no_good() {
    // e7, s9, h10, eJ, 3 ist Farbstich
    let r = Round {
        card_array: [
            Card { color: Color::EICHEL, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::NINE },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let mut test_knowledge = Knowledge::new();
    test_knowledge.set_rank_possibility_at(&Card { color: Color::EICHEL, rank: Rank::SEVEN },
                                           IMPOSSIBLE).expect("TODO: panic message");
    test_knowledge.set_rank_possibility_at(&Card { color: Color::SCHELL, rank: Rank::NINE },
                                           IMPOSSIBLE).expect("TODO: panic message");
    test_knowledge.set_rank_possibility_at(&Card { color: Color::HERZ, rank: Rank::TEN },
                                           IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(test_knowledge, knowledge);

    // e7, e9, h10, eJ, 1
    let r = Round {
        card_array: [
            Card { color: Color::EICHEL, rank: Rank::SEVEN },
            Card { color: Color::EICHEL, rank: Rank::NINE },
            Card { color: Color::HERZ, rank: Rank::TEN },
            Card { color: Color::EICHEL, rank: Rank::JACK }
        ],
        winner_index: 1,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let mut test_knowledge = Knowledge::new();
    test_knowledge.set_rank_possibility_at(&Card { color: Color::EICHEL, rank: Rank::SEVEN },
                                           IMPOSSIBLE).expect("TODO: panic message");
    test_knowledge.set_rank_possibility_at(&Card { color: Color::HERZ, rank: Rank::TEN },
                                           IMPOSSIBLE).expect("TODO: panic message");
    test_knowledge.set_rank_possibility_at(&Card { color: Color::EICHEL, rank: Rank::JACK },
                                           IMPOSSIBLE).expect("TODO: panic message");
    assert_eq!(test_knowledge, knowledge);

    // e7, s7, h7, l7, 1
    let r = Round {
        card_array: [
            Card { color: Color::EICHEL, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::SEVEN },
            Card { color: Color::HERZ, rank: Rank::SEVEN },
            Card { color: Color::LAUB, rank: Rank::SEVEN }
        ],
        winner_index: 1,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let test_knowledge = Knowledge::new();
    assert_eq!(test_knowledge, knowledge);

    // e7, s7, h7, l7, 3
    let r = Round {
        card_array: [
            Card { color: Color::EICHEL, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::SEVEN },
            Card { color: Color::HERZ, rank: Rank::SEVEN },
            Card { color: Color::LAUB, rank: Rank::SEVEN }
        ],
        winner_index: 3,
    };
    let mut knowledge = Knowledge::new();
    calculate_knowledge_from_round_no_good(&r, &mut knowledge);
    let test_knowledge = Knowledge::new();
    assert_eq!(test_knowledge, knowledge);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#[test]
fn test_calculate_winner_from_round() {
    // Rechter == eK
    let e_k = Card { color: Color::EICHEL, rank: Rank::KING };
    let mut knowledge = Knowledge::new();
    knowledge.set_rank_possibility_at(&e_k, SURE).expect("TODO: panic message");
    knowledge.set_color_possibility_at(&e_k, SURE).expect("TODO: panic message");

    // l7, s8, l10, sA, 2
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::EIGHT },
            Card { color: Color::LAUB, rank: Rank::TEN },
            Card { color: Color::SCHELL, rank: Rank::AS }
        ],
        winner_index: 2,
    };
    assert_eq!(2, calculate_winner_from_round(&r, &knowledge));

    // l10, s8, l7, sA, 2
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::TEN },
            Card { color: Color::SCHELL, rank: Rank::EIGHT },
            Card { color: Color::LAUB, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::AS }
        ],
        winner_index: 0,
    };
    assert_eq!(0, calculate_winner_from_round(&r, &knowledge));

    // l10, s8, h7, sA, 2
    let r = Round {
        card_array: [
            Card { color: Color::LAUB, rank: Rank::TEN },
            Card { color: Color::SCHELL, rank: Rank::EIGHT },
            Card { color: Color::HERZ, rank: Rank::SEVEN },
            Card { color: Color::SCHELL, rank: Rank::AS }
        ],
        winner_index: 0,
    };
    assert_eq!(0, calculate_winner_from_round(&r, &knowledge));
}

